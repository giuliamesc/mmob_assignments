# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 12:02:32 2021

@author: annap
"""

# Translated to .py by Meritxell Pacheco
# 2017
# Adapted to PandasBiogeme by Michel Bierlaire
# Sun Oct 21 23:15:31 2018
import numpy as np
import pandas as pd
import biogeme.database as db
import biogeme.biogeme as bio
from biogeme.models import loglogit
from biogeme.models import piecewiseFormula
from biogeme.models import lognested
from biogeme.expressions import Beta, DefineVariable, log
from biogeme.models import loglogit
from biogeme.expressions import Beta, DefineVariable, log

pandas = pd.read_table("lpmc03.dat")
database = db.Database("travel",pandas)
pd.options.display.float_format = '{:.3g}'.format

globals().update(database.variables)


mean_w = np.mean(pandas["dur_walking"].where(pandas["travel_mode"]==1))
mean_cy = np.mean(pandas["dur_cycling"].where(pandas["travel_mode"]==2))
mean_pt = np.mean(pandas["dur_pt_access"].where(pandas["travel_mode"]==3))+np.mean(pandas["dur_pt_rail"].where(pandas["travel_mode"]==3)) + np.mean(pandas["dur_pt_bus"].where(pandas["travel_mode"]==3))+np.mean(pandas["dur_pt_int"].where(pandas["travel_mode"]==3))
mean_car = np.mean(pandas["dur_driving"].where(pandas["travel_mode"]==4))

#exclude = (  driving_license   ==  0  )
#database.remove(exclude)

# Choice
#travel_mode = 1 : walking
#travel_mode = 2 : cycling
#travel_mode = 3 : public transport
#travel_mode = 4 : car
chosenAlternative = travel_mode

#Parameters to be estimated
# Arguments:
#   1  Name for report. Typically, the same as the variable
#   2  Starting value
#   3  Lower bound
#   4  Upper bound
#   5  0: estimate the parameter, 1: keep it fixed
Constant1	 = Beta('Constant1',0,None,None,1)
Constant2	 = Beta('Constant2',0,None,None,0)
Constant3	 = Beta('Constant3',0,None,None,0)
Constant4	 = Beta('Constant4',0,None,None,0)
Tot_time_1	 = Beta('Tot_time_1',0,None,None,0)
Tot_time_2	 = Beta('Tot_time_2',0,None,None,0)
Tot_time_3	 = Beta('Tot_time_3',0,None,None,0)
Tot_time_4	 = Beta('Tot_time_4',0,None,None,0)

Cost	 = Beta('Cost',0,None,None,0)

LAMBDAW = Beta('LAMBDAW',1,None,None,0)
LAMBDAC = Beta('LAMBDAC',1,None,None,0)
LAMBDAPT = Beta('LAMBDAPT',1,None,None,0)
LAMBDACAR = Beta('LAMBDACAR',1,None,None,0)


#socioeconomic variable 
Traffic = Beta('Traffic',0,None,None,0) #significative
Female_2 = Beta('Female_2',0,None,None,0) #significative
LICENSE = Beta('LICENSE',0,None,None,0)

# parameters relevant to the nests
MU_MOT = Beta('MU_MOT',1,1,None,0)
MU_CY = Beta('MU_CY',1,1,None, 1)
MU_W = Beta('MU_W',1,1,None,1)


# Define here arithmetic expressions for name that are not directly
# available from the data

Cost_car = DefineVariable('Cost_car', cost_driving_fuel+cost_driving_ccharge, database)
Cost_car = 1.15*Cost_car
Cost_pt = DefineVariable('Cost_pt', cost_transit, database)
Dur_car = DefineVariable("Dur_car", dur_driving/mean_car, database)
Dur_w = DefineVariable("Dur_w", dur_walking/mean_w, database)
Dur_cy = DefineVariable("Dur_cy", dur_cycling/mean_cy, database)
Dur_pt = DefineVariable("Dur_pt", (dur_pt_access+dur_pt_rail+dur_pt_bus+dur_pt_int)/mean_pt, database)
Age = DefineVariable("Age", age, database)

thresholds_pt = [None, 25 , 55, None]
thresholds_b = [None, 15 , 45, None]
init_Ages_pt = [0,0,0]
init_Ages_b = [0,0,0]

# Utilities
Opt1 = Constant1 + Tot_time_1 * ((Dur_w ** LAMBDAW - 1) / LAMBDAW)  
Opt2 = Constant2 + Tot_time_2 * ((Dur_cy ** LAMBDAC -1) / LAMBDAC) + Female_2 * female + piecewiseFormula(Age, thresholds_b, init_Ages_b)
Opt3 = Constant3 + Tot_time_3 * ((Dur_pt ** LAMBDAPT - 1) / LAMBDAPT)   + Cost * Cost_pt + piecewiseFormula(Age, thresholds_pt, init_Ages_pt)
Opt4 = Constant4 + Tot_time_4 * ((Dur_car ** LAMBDACAR - 1) / LAMBDACAR)   + Cost * Cost_car+ Traffic * driving_traffic_percent + LICENSE*driving_license 



V = {1: Opt1,2: Opt2,3: Opt3, 4:Opt4}
av = {1: 1,2: 1,3: 1, 4: 1}

#Definitions of nests
MU_MOT = MU_MOT, [3,4]
MU_W = MU_W, [1]
MU_CY = MU_CY, [2]

nests = MU_MOT, MU_W, MU_CY

# NL
logprob = lognested(V, av, nests, chosenAlternative)
biogeme  = bio.BIOGEME(database,logprob)
biogeme.modelName = "LPCM_NL_unrestricted_boxcox1"
results = biogeme.estimate()

# Get the results in a pandas table
pandasResults = results.getEstimatedParameters()
display(pandasResults)
print(f"Nbr of observations: {database.getNumberOfObservations()}")
print(f"LL(0) =    {results.data.initLogLike:.3f}")
print(f"LL(beta) = {results.data.logLike:.3f}")
print(f"rho bar square = {results.data.rhoBarSquare:.3g}")
print(f"Output file: {results.data.htmlFileName}")

# Compare with the logit model
logprob_logit = loglogit(V,av,chosenAlternative)
biogeme_logit  = bio.BIOGEME(database,logprob_logit)
biogeme_logit.modelName = "LPCM_generic_linear"
results_logit = biogeme_logit.estimate()

ll_logit = results_logit.data.logLike
rhobar_logit = results_logit.data.rhoBarSquare
ll_nested = results.data.logLike
rhobar_nested = results.data.rhoBarSquare

print(f"LL logit:  {ll_logit:.3f}  rhobar: {rhobar_logit:.3f}  Parameters: {results_logit.data.nparam}")
print(f"LL nested: {ll_nested:.3f}  rhobar: {rhobar_nested:.3f}  Parameters: {results.data.nparam}")
lr = -2 * (ll_logit - ll_nested)
print(f"Likelihood ratio: {lr:.3f}")
