# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 18:55:52 2021

@author: giuli
"""
import numpy as np
import pandas as pd
import biogeme.database as db
import biogeme.biogeme as bio
from biogeme.models import loglogit
from biogeme.models import piecewiseFormula
from biogeme.models import lognested
from biogeme.expressions import Beta, DefineVariable, log, Derive
from biogeme.models import loglogit
from biogeme.expressions import Beta, DefineVariable, log
import biogeme.results as res
import biogeme.models as models

pandas = pd.read_table("lpmc03_w.dat")
database = db.Database("lpmc03",pandas)
pd.options.display.float_format = '{:.3g}'.format

globals().update(database.variables)

#WEIGHT NORMALIZATION
sumWeights = database.data['Weights'].sum()
print(database.data['Weights'].sum())
S= database.getSampleSize()
sampleNormalizedWeight = Weights * S / sumWeights

# MODEL SPECIFICATION
mean_w = np.mean(pandas["dur_walking"].where(pandas["travel_mode"]==1))
mean_cy = np.mean(pandas["dur_cycling"].where(pandas["travel_mode"]==2))
mean_pt = np.mean(pandas["dur_pt_access"].where(pandas["travel_mode"]==3))+np.mean(pandas["dur_pt_rail"].where(pandas["travel_mode"]==3)) + np.mean(pandas["dur_pt_bus"].where(pandas["travel_mode"]==3))+np.mean(pandas["dur_pt_int"].where(pandas["travel_mode"]==3))
mean_car = np.mean(pandas["dur_driving"].where(pandas["travel_mode"]==4))

Constant1	 = Beta('Constant1',0,None,None,1)
Constant2	 = Beta('Constant2',0,None,None,0)
Constant3	 = Beta('Constant3',0,None,None,0)
Constant4	 = Beta('Constant4',0,None,None,0)
Tot_time_1	 = Beta('Tot_time_1',0,None,None,0)
Tot_time_2	 = Beta('Tot_time_2',0,None,None,0)
Tot_time_3	 = Beta('Tot_time_3',0,None,None,0)
Tot_time_4	 = Beta('Tot_time_4',0,None,None,0)

Cost	 = Beta('Cost',0,None,None,0)

LAMBDAW = Beta('LAMBDAW',1,None,None,0)
LAMBDAC = Beta('LAMBDAC',1,None,None,0)
LAMBDAPT = Beta('LAMBDAPT',1,None,None,0)
LAMBDACAR = Beta('LAMBDACAR',1,None,None,0)

#socioeconomic variable 
Traffic = Beta('Traffic',0,None,None,0) #significative
Female_2 = Beta('Female_2',0,None,None,0) #significative
LICENSE = Beta('LICENSE',0,None,None,0)

# parameters relevant to the nests
MU_MOT = Beta('MU_MOT',1,1,None,0)
MU_CY = Beta('MU_CY',1,1,None, 1)
MU_W = Beta('MU_W',1,1,None,1)


# Define here arithmetic expressions for name that are not directly
# available from the data

Cost_car = DefineVariable('Cost_car', cost_driving_fuel+cost_driving_ccharge, database)
Cost_car = 1.15*Cost_car
Cost_pt = DefineVariable('Cost_pt', cost_transit, database)
Dur_car = DefineVariable("Dur_car", dur_driving/mean_car, database)
Dur_w = DefineVariable("Dur_w", dur_walking/mean_w, database)
Dur_cy = DefineVariable("Dur_cy", dur_cycling/mean_cy, database)
Dur_pt = DefineVariable("Dur_pt", (dur_pt_access+dur_pt_rail+dur_pt_bus+dur_pt_int)/mean_pt, database)
Age = DefineVariable("Age", age, database)
Weight = DefineVariable("Weight", Weights, database)

thresholds_pt = [None, 25 , 55, None]
thresholds_b = [None, 15 , 45, None]
init_Ages_pt = [0,0,0]
init_Ages_b = [0,0,0]

# Utilities
Opt1 = Constant1 + Tot_time_1 * ((Dur_w ** LAMBDAW - 1) / LAMBDAW)  
Opt2 = Constant2 + Tot_time_2 * ((Dur_cy ** LAMBDAC -1) / LAMBDAC) + Female_2 * female + piecewiseFormula(Age, thresholds_b, init_Ages_b)
Opt3 = Constant3 + Tot_time_3 * ((Dur_pt ** LAMBDAPT - 1) / LAMBDAPT)   + Cost * Cost_pt + piecewiseFormula(Age, thresholds_pt, init_Ages_pt)
Opt4 = Constant4 + Tot_time_4 * ((Dur_car ** LAMBDACAR - 1) / LAMBDACAR)   + Cost * Cost_car+ Traffic * driving_traffic_percent + LICENSE*driving_license 


V = {1: Opt1,2: Opt2,3: Opt3, 4:Opt4}
av = {1: 1,2: 1,3: 1, 4: 1}

#Definitions of nests
MU_MOT = MU_MOT, [3,4]
MU_W = MU_W, [1]
MU_CY = MU_CY, [2]

nests = MU_MOT, MU_W, MU_CY

#SIMULATION
prob_w = models.nested(V,av,nests,1)
prob_cy = models.nested(V,av,nests,2)
prob_pt = models.nested(V,av,nests,3)
prob_car = models.nested(V,av,nests,4)
VOT_CAR = Derive(Opt4, 'Dur_car')/Derive(Opt4, 'Cost_car')
VOT_PT = Derive(Opt3, 'Dur_pt')/Derive(Opt3, 'Cost_pt')
elast_car_cost = Derive(prob_car, 'Cost_car') * Cost_car/prob_car
elast_pt_cost= Derive(prob_pt, 'Cost_pt') * Cost_pt/prob_pt
elast_car_cross = Derive(prob_car, 'Cost_pt') * Cost_pt / prob_car
elast_pt_cross = Derive(prob_pt, 'Cost_car') * Cost_car / prob_pt

simulate = {
'Prob. w': prob_w,
'Prob. cy': prob_cy,
'Prob. pt': prob_pt,
'Prob. car': prob_car,
'VOT_CAR': VOT_CAR,
'VOT_PT': VOT_PT,
'elast_car_cost' : elast_car_cost,
'elast_pt_cost': elast_pt_cost,
'elast_car_cross': elast_car_cross,
'elast_pt_cross': elast_pt_cross,
'Weight': Weight,
'Weighted prob. w': sampleNormalizedWeight * prob_w,
'Weighted prob. cy': sampleNormalizedWeight * prob_cy,
'Weighted prob. pt': sampleNormalizedWeight * prob_pt,
'Weighted prob. car': sampleNormalizedWeight * prob_car,
'Weighted VOT CAR': sampleNormalizedWeight * VOT_CAR,
'Weighted VOT PT': sampleNormalizedWeight * VOT_PT
}


biogeme = bio.BIOGEME(database, simulate)
biogeme.modelName = "LPMC_Base_Simul"

betas = biogeme.freeBetaNames
results = res.bioResults(pickleFile = 'LPCM_NL_unrestricted_boxcox.pickle')
betaValues = results.getBetaValues()

simulatedValues = biogeme.simulate(betaValues)

# Calculate confidence intervals
b = results.getBetasForSensitivityAnalysis(biogeme.freeBetaNames, size=100, useBootstrap=False)
left, right = biogeme.confidenceIntervals(b, 0.9)

# Normalization factors
normalization_car = simulatedValues['Weighted prob. car'].sum()
normalization_pt = simulatedValues['Weighted prob. pt'].sum()

print('Normalization car: {:.3f}'.format(normalization_car))
print('Normalization pt: {:.3f}'.format(normalization_pt))

# Aggregate Elasticities

agg_elast_car_cost_direct = (simulatedValues['Weighted prob. car'] * simulatedValues['elast_car_cost'] / normalization_car).sum()
agg_elast_pt_cost_direct = (simulatedValues['Weighted prob. pt'] * simulatedValues['elast_pt_cost'] / normalization_pt).sum()
agg_elast_car_cost_cross = (simulatedValues['Weighted prob. car'] * simulatedValues['elast_car_cross'] / normalization_car).sum()
agg_elast_pt_cost_cross = (simulatedValues['Weighted prob. pt'] * simulatedValues['elast_pt_cost'] / normalization_pt).sum()


print('Elasticity of car wrt cost: {:.4f}'.format(agg_elast_car_cost_direct))
print('Elasticity of pt wrt cost: {:.4f}'.format(agg_elast_pt_cost_direct))
print('Elasticity of car wrt cost pt: {:.4f}'.format(agg_elast_car_cost_cross))
print('Elasticity of pt wrt cost car: {:.4f}'.format(agg_elast_pt_cost_cross))


## VOT 

# VOT without intervals

vot_car = simulatedValues['Weighted VOT CAR'].mean()
vot_pt = simulatedValues['Weighted VOT PT'].mean()

print('Value of time car: {:4f} eur/h'.format(vot_car))
print('Value of time pt: {:4f} eur/h'.format(vot_pt))

# VOT with confidence intervals

left['Weighted VOT CAR'] = left['VOT_CAR'] * left['Weight']
right['Weighted VOT CAR'] = right['VOT_CAR'] * right['Weight']

left['Weighted VOT PT'] = left['VOT_PT'] * left['Weight']
right['Weighted VOT PT'] = right['VOT_PT'] * right['Weight']

avg_VOT_PT = simulatedValues['Weighted VOT PT'].sum() / simulatedValues['Weight'].sum()
avg_VOT_PT_left = left['Weighted VOT PT'].sum() / left['Weight'].sum()
avg_VOT_PT_right = right['Weighted VOT PT'].sum() / right['Weight'].sum()
avg_VOT_Car = simulatedValues['Weighted VOT CAR'].sum() / simulatedValues['Weight'].sum()
avg_VOT_Car_left = left['Weighted VOT CAR'].sum() / left['Weight'].sum()
avg_VOT_Car_right = right['Weighted VOT CAR'].sum() / right['Weight'].sum()


print(f'Average VOT for public transp.: {avg_VOT_PT:.4f} £/h '
      f'[{avg_VOT_PT_left:.4f}:{avg_VOT_PT_right:.4f}]')
print(f'Average VOT for car           : {avg_VOT_Car:.4f} £/h '
      f'[{avg_VOT_Car_left:.4f}:{avg_VOT_Car_right:.4f}]')

## Market Shares

# MS without intervals

marketShare_w = 100 * simulatedValues['Weighted prob. w'].mean()
marketShare_cy = 100 * simulatedValues['Weighted prob. cy'].mean()
marketShare_pt = 100 * simulatedValues['Weighted prob. pt'].mean()
marketShare_car = 100 * simulatedValues['Weighted prob. car'].mean()

print('Market share w: {} %'.format(marketShare_w))
print('Market share cy: {} %'.format(marketShare_cy))
print('Market share pt: {} %'.format(marketShare_pt))
print('Market share car: {} %'.format(marketShare_car))

# MS with confidence intervals

simulatedValues['Weighted prob. pt'] = simulatedValues['Prob. pt'] * simulatedValues['Weight']
left['Weighted prob. pt'] = left['Prob. pt'] * left['Weight']
right['Weighted prob. pt'] = right['Prob. pt'] * right['Weight']
simulatedValues['Weighted prob. car'] = simulatedValues['Prob. car'] * simulatedValues['Weight']
left['Weighted prob. car'] = left['Prob. car'] * left['Weight']
right['Weighted prob. car'] = right['Prob. car'] * right['Weight']

pt = (left['Weighted prob. pt'].sum() / sumWeights, simulatedValues['Weighted prob. pt'].sum() / sumWeights, right['Weighted prob. pt'].sum() / sumWeights)
car = (left['Weighted prob. car'].sum() / sumWeights, simulatedValues['Weighted prob. car'].sum() / sumWeights, right['Weighted prob. car'].sum() / sumWeights)
    
print(f'MS for public transp.: {pt[1]:4f}'
      f'[{pt[0]:4f}:{pt[2]:4f}]')
print(f'MS for car           : {car[1]:4f}'
      f'[{car[0]:4f}:{car[2]:4f}]')

## Comparison with the actual choices - RIGHT

simulatedValues['simulated choice'] = simulatedValues.apply(lambda row: 1*(max(row['Prob. pt'], row['Prob. car'], row['Prob. w'], row['Prob. cy']) == row['Prob. w']) + 2*(max(row['Prob. pt'], row['Prob. car'], row['Prob. w'], row['Prob. cy']) == row['Prob. cy']) + 3*(max(row['Prob. pt'], row['Prob. car'], row['Prob. w'], row['Prob. cy']) == row['Prob. pt']) + 4*(max(row['Prob. pt'], row['Prob. car'], row['Prob. w'], row['Prob. cy']) == row['Prob. car']) , axis = 1)
diff = simulatedValues['simulated choice'] - pandas.travel_mode
pandas['diff'] = diff

sim_wrong = np.zeros([4,])

for i in np.arange(1,5):
    sim_wrong[i-1] = 100*np.count_nonzero((pandas['diff'] != 0) & (pandas['travel_mode'] == i))/S
    print('Share of users choosing alternative {} with a higher probability for another alternative: {} %'.format(i,sim_wrong[i-1]))

## Comparison with the actual choices - WRONG

# choice_w = np.count_nonzero(pandas["travel_mode"]==1)/S
# choice_cy = np.count_nonzero(pandas["travel_mode"]==2)/S
# choice_pt = np.count_nonzero(pandas["travel_mode"]==3)/S
# choice_car = np.count_nonzero(pandas["travel_mode"]==4)/S

# print('Percentual actual choice of w: {} '.format(choice_w))
# print('Percentual actual choice of cy: {} '.format(choice_cy))
# print('Percentual actual choice of pt: {} '.format(choice_pt))
# print('Percentual actual choice of car: {} '.format(choice_car))

## Total revenue of public transportation

pt_revenue = np.zeros([3,])

pt_revenue[1] = sum(pandas["cost_transit"]*simulatedValues['Weighted prob. pt'])
pt_revenue[0] = sum(pandas["cost_transit"]*left['Weighted prob. pt']) 
pt_revenue[2] = sum(pandas["cost_transit"]*right['Weighted prob. pt'])

print(f'Predicted total revenue for public transp.: {pt_revenue[1]:4f}'
      f'[{pt_revenue[0]:4f}:{pt_revenue[2]:4f}]')
