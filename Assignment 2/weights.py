# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 18:47:56 2021

@author: giuli
"""
import pandas as pd
import numpy as np

df = pd.read_table('lpmc03.dat')

df.head()

age1 = (df.age <= 40)
female = df.female

s1 = df[age1 & female]
s2 = df[~age1 & female]
s3 = df[age1 & ~female]
s4 = df[~age1 & ~female]

print('Strata sizes: ')
print('s1 = {}'.format(len(s1.index)))
print('s2 = {}'.format(len(s2.index)))
print('s3 = {}'.format(len(s3.index)))
print('s4 = {}'.format(len(s4.index)))

n_g = [2599058, 1765143, 2676249, 1633263]
s_g = [len(s1.index), len(s2.index), len(s3.index), len(s4.index)]

N = sum(n_g)
S = sum(s_g)

weights = []

print('Weights:')
for i, (ng, sg) in enumerate(zip(n_g, s_g)):
    w = ng*S / (sg*N)
    weights.append(w)
    print('w{} = {}'.format(i+1, w))
    
s1 = s1.assign(Weights= weights[0])
s2 = s2.assign(Weights= weights[1])
s3 = s3.assign(Weights= weights[2])
s4 = s4.assign(Weights= weights[3])

strata = [s1, s2, s3, s4]
df = pd.concat(strata).sort_index()

df.to_csv('lpmc03_w.dat', sep = '\t', index = False)