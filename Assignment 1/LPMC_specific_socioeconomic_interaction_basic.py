# Translated to .py by Meritxell Pacheco
# 2017
# Adapted to PandasBiogeme by Michel Bierlaire
# Sun Oct 21 23:15:31 2018
import numpy as np
import pandas as pd
import biogeme.database as db
import biogeme.biogeme as bio
from biogeme.expressions import Beta, DefineVariable
from biogeme.models import loglogit

pandas = pd.read_table("lpmc03.dat")
database = db.Database("travel",pandas)
pd.options.display.float_format = '{:.3g}'.format

globals().update(database.variables)

# Choice
#travel_mode = 1 : walking
#travel_mode = 2 : cycling
#travel_mode = 3 : public transport
#travel_mode = 4 : car
chosenAlternative = travel_mode

#Parameters to be estimated
# Arguments:
#   1  Name for report. Typically, the same as the variable
#   2  Starting value
#   3  Lower bound
#   4  Upper bound
#   5  0: estimate the parameter, 1: keep it fixed
Constant1	 = Beta('Constant1',0,None,None,1)
Constant2	 = Beta('Constant2',0,None,None,0)
Constant3	 = Beta('Constant3',0,None,None,0)
Constant4	 = Beta('Constant4',0,None,None,0)
Tot_time_1	 = Beta('Tot_time_1',0,None,None,0)
Tot_time_2	 = Beta('Tot_time_2',0,None,None,0)
Tot_time_3	 = Beta('Tot_time_3',0,None,None,0)
Tot_time_4	 = Beta('Tot_time_4',0,None,None,0)
Cost	 = Beta('Cost',0,None,None,0)


#socioeconomic variable 
# Age_2 = Beta('Age_2',0,None,None,0) #significative
# Age_3 = Beta('Age_3',0,None,None,0) #significative
# Age_4 = Beta('Age_4',0,None,None,0) #significative
Traffic = Beta('Traffic',0,None,None,0) #significative
Female_2 = Beta('Female_2',0,None,None,0) #significative
LICENSE = Beta('LICENSE',0,None,None,0) 


# Define here arithmetic expressions for name that are not directly
# available from the data

Cost_car = DefineVariable('Cost_car', cost_driving_fuel+cost_driving_ccharge, database)
Cost_pt = DefineVariable('Cost_pt', cost_transit, database)
Dur_car = DefineVariable("Dur_car", dur_driving, database)
Dur_w = DefineVariable("Dur_w", dur_walking, database)
Dur_cy = DefineVariable("Dur_cy", dur_cycling, database)
Dur_pt = DefineVariable("Dur_pt", dur_pt_access+dur_pt_rail+dur_pt_bus+dur_pt_int, database)

# Utilities
Opt1 = Constant1 + Tot_time_1 * Dur_w  
Opt2 = Constant2 + Tot_time_2 * Dur_cy + Female_2 * female
Opt3 = Constant3 + Tot_time_3 * Dur_pt + Cost * Cost_pt
Opt4 = Constant4 + Tot_time_4 * Dur_car + Cost * Cost_car + Traffic * driving_traffic_percent + LICENSE * driving_license



V = {1: Opt1,2: Opt2,3: Opt3, 4:Opt4}
av = {1: 1,2: 1,3: 1, 4: 1}

# The choice model is a logit, with availability conditions
logprob = loglogit(V,av,chosenAlternative)
biogeme  = bio.BIOGEME(database,logprob)
biogeme.modelName = "logit_travelchoice_specific_basic"
results = biogeme.estimate()
# Get the results in a pandas table
pandasResults = results.getEstimatedParameters()
print(pandasResults)
print(f"Nbr of observations: {database.getNumberOfObservations()}")
print(f"LL(0) =    {results.data.initLogLike:.3f}")
print(f"LL(beta) = {results.data.logLike:.3f}")
print(f"rho bar square = {results.data.rhoBarSquare:.3g}")
print(f"Output file: {results.data.htmlFileName}")

# In[ ]:
